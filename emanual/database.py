import sqlite3


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class DBManager(object):

    def __init__(self):
        self.conn = sqlite3.connect('emanual_db.sqlite3')
        self.conn.row_factory = dict_factory
        print("Opened database successfully")

    def drop_tables(self):
        self.conn.execute("DROP TABLE IF EXISTS UserManual;")
        self.conn.execute("DROP TABLE IF EXISTS SourceSentence;")
        self.conn.execute("DROP TABLE IF EXISTS SentenceSchema;")
        self.conn.execute("DROP TABLE IF EXISTS SentenceElement;")
        self.conn.execute("DROP TABLE IF EXISTS Dependency;")
        self.conn.commit()

    def create_tables(self):
        self.conn.execute('''CREATE TABLE IF NOT EXISTS UserManual (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name CHAR(1000)
            )''')

        self.conn.execute('''CREATE TABLE IF NOT EXISTS SourceSentence (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            text TEXT NOT NULL,
            section CHAR(50),
            page_num INT,
            user_manual_id INT NOT NULL
            )''')
        self.conn.execute('''CREATE INDEX SourceSentence_user_manual_id_index ON SourceSentence('user_manual_id')''')

        self.conn.execute('''CREATE TABLE IF NOT EXISTS SentenceSchema (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            schema TEXT NOT NULL,
            source_sentence_id INT NOT NULL
            )''')
        self.conn.execute('''CREATE INDEX SentenceSchema_source_sentence_id_index ON SentenceSchema('source_sentence_id')''')

        self.conn.execute('''CREATE TABLE IF NOT EXISTS SentenceElement (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            text CHAR(500),
            pos CHAR(10),
            role CHAR(50),
            source_sentence_id INT NOT NULL
            )''')
        self.conn.execute('''CREATE INDEX SentenceElement_source_sentence_id_index ON SentenceElement('source_sentence_id')''')

        self.conn.execute('''CREATE TABLE IF NOT EXISTS Dependency (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            element_from INT NOT NULL,
            element_to INT NOT NULL
            )''')

        self.conn.execute('''CREATE TABLE ConvertedSentence (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            source_sentence_id INT NOT NULL,
            acomp CHAR(1000),
            advcl CHAR(1000),
            advmod CHAR(1000),
            agent CHAR(1000),
            amod CHAR(1000),
            appos CHAR(1000),
            aux CHAR(1000),
            auxpass CHAR(1000),
            cc CHAR(1000),
            ccomp CHAR(1000),
            conj CHAR(1000),
            cop CHAR(1000),
            csubj CHAR(1000),
            csubjpass CHAR(1000),
            dep CHAR(1000),
            det CHAR(1000),
            discourse CHAR(1000),
            dobj CHAR(1000),
            expl CHAR(1000),
            goeswith CHAR(1000),
            iobj CHAR(1000),
            mark CHAR(1000),
            mwe CHAR(1000),
            neg CHAR(1000),
            nn CHAR(1000),
            npadvmod CHAR(1000),
            nsubj CHAR(1000),
            nsubjpass CHAR(1000),
            num CHAR(1000),
            "number" CHAR(1000),
            parataxis CHAR(1000),
            pcomp CHAR(1000),
            pobj CHAR(1000),
            poss CHAR(1000),
            possessive CHAR(1000),
            preconj CHAR(1000),
            predet CHAR(1000),
            prep CHAR(1000),
            prepc CHAR(1000),
            prt CHAR(1000),
            punct CHAR(1000),
            quantmod CHAR(1000),
            rcmod CHAR(1000),
            "ref" CHAR(1000),
            root CHAR(1000),
            tmod CHAR(1000),
            vmod CHAR(1000),
            xcomp CHAR(1000),
            xsubj CHAR(1000)
        )''')
        self.conn.execute('''CREATE INDEX ConvertedSentence_source_sentence_id_index ON ConvertedSentence('source_sentence_id')''')

        self.conn.execute('''CREATE TABLE IF NOT EXISTS PronounDependency (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            converted_sentence_from INT NOT NULL,
            converted_sentence_to INT NOT NULL,
            role_from CHAR(1000),
            role_to CHAR(1000)
        )''')

        self.conn.commit()
        print("Table created successfully")

    def save_user_manual(self, name=None):
        name, = self._convert_to_safetexts((name,))
        sql = "INSERT INTO UserManual (name) VALUES ('%s')" % name
        return self._save(sql)

    def retrieve_user_manual(self, user_manual_id=None, name=None):
        name, = self._convert_to_safetexts((name,))
        if user_manual_id is not None and name is not None:
            sql = "SELECT * FROM UserManual WHERE id=%s AND name='%s'" % (user_manual_id, name)
        elif user_manual_id is not None:
            sql = "SELECT * FROM UserManual WHERE id=%s" % (user_manual_id)
        elif name is not None:
            sql = "SELECT * FROM UserManual WHERE name='%s'" % (name)
        else:
            sql = "SELECT * FROM UserManual"
        return self._retrieve(sql)

    def save_source_sentence(self, text=None, section=None, page_num=None, user_manual_id=None):
        text, section = self._convert_to_safetexts((text, section))
        if section is None:
            section = ""
        if page_num is None:
            page_num = 0
        sql = "INSERT INTO SourceSentence (text, section, page_num, user_manual_id) VALUES ('%s', '%s', %s, %s)" % (text, section, page_num, user_manual_id)
        return self._save(sql)

    def retrieve_source_sentence(self, source_sentence_id=None, user_manual_id=None):
        if source_sentence_id is not None and user_manual_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE id=%s AND user_manual_id='%s'" % (source_sentence_id, user_manual_id)
        elif source_sentence_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE id=%s" % (source_sentence_id)
        elif user_manual_id is not None:
            sql = "SELECT * FROM SourceSentence WHERE user_manual_id='%s'" % (user_manual_id)
        else:
            sql = "SELECT * FROM SourceSentence"
        return self._retrieve(sql)

    def save_sentence_schema(self, schema=None, source_sentence_id=None):
        schema, = self._convert_to_safetexts((schema,))
        sql = 'INSERT INTO SentenceSchema (schema, source_sentence_id) VALUES ("%s", %s)' % (schema, source_sentence_id)
        return self._save(sql)

    def retrieve_sentence_schema(self, schema_id=None, source_sentence_id=None):
        if schema_id is not None and source_sentence_id is not None:
            sql = 'SELECT * FROM SentenceSchema WHERE id=%s AND source_sentence_id="%s"' % (schema_id, source_sentence_id)
        elif schema_id is not None:
            sql = 'SELECT * FROM SentenceSchema WHERE id=%s' % (schema_id)
        elif source_sentence_id is not None:
            sql = 'SELECT * FROM SentenceSchema WHERE source_sentence_id="%s"' % (source_sentence_id)
        else:
            sql = 'SELECT * FROM SentenceSchema'
        return self._retrieve(sql)

    def save_element(self, text=None, pos=None, role=None, source_sentence_id=None):
        text, pos, role = self._convert_to_safetexts((text, pos, role))
        sql = "INSERT INTO SentenceElement (text, pos, role, source_sentence_id) VALUES ('%s', '%s', '%s', %s)" % (text, pos, role, source_sentence_id)
        return self._save(sql)

    def retrieve_elements(self, element_id=None, source_sentence_id=None):
        if element_id is not None and source_sentence_id is not None:
            sql = "SELECT * FROM SentenceElement WHERE id=%s AND source_sentence_id='%s'" % (element_id, source_sentence_id)
        elif element_id is not None:
            sql = "SELECT * FROM SentenceElement WHERE id=%s" % (element_id)
        elif source_sentence_id is not None:
            sql = "SELECT * FROM SentenceElement WHERE source_sentence_id='%s'" % (source_sentence_id)
        else:
            sql = "SELECT * FROM SentenceElement"
        return self._retrieve(sql)

    def save_dependency(self, element_from=None, element_to=None):
        sql = "INSERT INTO Dependency (element_from, element_to) VALUES (%s, %s)" % (element_from, element_to)
        return self._save(sql)

    def retrieve_dependencies(self, dependency_id=None, element_from=None, element_to=None):
        if dependency_id is not None:
            sql = "SELECT * FROM Dependency WHERE id=%s" % (dependency_id)
        elif element_from is not None and element_to is not None:
            sql = "SELECT * FROM Dependency WHERE element_from=%s AND element_to=%s" % (element_from, element_to)
        elif element_from is not None:
            sql = "SELECT * FROM Dependency WHERE element_from=%s" % (element_from)
        elif element_from is not None:
            sql = "SELECT * FROM Dependency WHERE element_to=%s" % (element_to)
        else:
            sql = "SELECT * FROM Dependency"
        return self._retrieve(sql)

    def retrieve_dependencies(self, dependency_id=None, element_from=None, element_to=None):
        if dependency_id is not None:
            sql = "SELECT * FROM Dependency WHERE id=%s" % (dependency_id)
        elif element_from is not None and element_to is not None:
            sql = "SELECT * FROM Dependency WHERE element_from=%s AND element_to=%s" % (element_from, element_to)
        elif element_from is not None:
            sql = "SELECT * FROM Dependency WHERE element_from=%s" % (element_from)
        elif element_from is not None:
            sql = "SELECT * FROM Dependency WHERE element_to=%s" % (element_to)
        else:
            sql = "SELECT * FROM Dependency"
        return self._retrieve(sql)

    def save_converted_sentence(self, source_sentence_id=None, roles=None):
        sql = "INSERT INTO ConvertedSentence (source_sentence_id"
        # decide columns
        values = []
        for role in roles:
            if role is None:
                continue
            sql += ","+role
            values.append(roles[role])
        sql += ') VALUES (%s' % source_sentence_id
        for value in values:
            value, = self._convert_to_safetexts((value,))
            sql += ',"'+value+'"'
        sql += ')'
        return self._save(sql)

    def retrieve_converted_sentences(self, converted_sentence_id=None, source_sentence_id=None, roles=None):
        if converted_sentence_id is not None:
            sql = "SELECT * FROM ConvertedSentence WHERE id=%s" % (converted_sentence_id)
        elif source_sentence_id is not None and roles is not None and len(roles) > 0:
            sql = "SELECT * FROM ConvertedSentence WHERE source_sentence_id=%s" % (converted_sentence_id)
            for k, v in roles.items():
                sql += " AND %s=%s" % (k, v)
        elif source_sentence_id is not None and roles is not None and len(roles) > 0:
            sql = "SELECT * FROM ConvertedSentence WHERE source_sentence_id=%s" % (converted_sentence_id)
            for k, v in roles.items():
                sql += " AND %s=%s" % (k, v)
        elif source_sentence_id is not None:
            sql = "SELECT * FROM ConvertedSentence WHERE source_sentence_id=%s" % (converted_sentence_id)
        else:
            sql = "SELECT * FROM ConvertedSentence"
        return self._retrieve(sql)

    def save_pronoun_dependency(self, converted_sentence_from=None, role_from=None, converted_sentence_to=None, role_to=None):
        role_from, role_to = self._convert_to_safetexts((role_from, role_to))
        sql = "INSERT INTO PronounDependency (converted_sentence_from, converted_sentence_to, role_from, role_to) " \
              "VALUES (%s, %s, '%s', '%s')" % (converted_sentence_from, converted_sentence_to, role_from, role_to)
        return self._save(sql)

    def retrieve_pronoun_dependencies(self, pronoun_dependency_id=None, converted_sentence_from=None, role_from=None, converted_sentence_to=None, role_to=None):
        if pronoun_dependency_id is not None:
            sql = "SELECT * FROM PronounDependency WHERE id=%s" % (pronoun_dependency_id)
        elif converted_sentence_from is not None and converted_sentence_to is not None:
            sql = "SELECT * FROM PronounDependency WHERE converted_sentence_from=%s AND converted_sentence_to=%s " \
                  "AND role_from='%s' AND role_to='%s'" % (
                converted_sentence_from, converted_sentence_to, role_from, role_to)
        elif converted_sentence_from is not None and converted_sentence_to is not None:
            sql = "SELECT * FROM PronounDependency WHERE converted_sentence_from=%s AND converted_sentence_to=%s" % (converted_sentence_from, converted_sentence_to)
        elif converted_sentence_from is not None and role_from is not None:
            sql = "SELECT * FROM PronounDependency WHERE converted_sentence_from=%s AND role_from='%s'" % (converted_sentence_from, role_from)
        elif converted_sentence_from is not None:
            sql = "SELECT * FROM PronounDependency WHERE converted_sentence_from=%s" % (converted_sentence_from)
        elif converted_sentence_to is not None and role_to is not None:
            sql = "SELECT * FROM PronounDependency WHERE converted_sentence_from=%s AND role_from='%s'" % (converted_sentence_from, role_from)
        elif converted_sentence_to is not None:
            sql = "SELECT * FROM PronounDependency WHERE converted_sentence_from=%s" % (converted_sentence_from)
        else:
            sql = "SELECT * FROM PronounDependency"
        return self._retrieve(sql)

    def _convert_to_safetexts(self, texts):
        return (text.replace("'", "''") if text is not None else 'None' for text in texts)

    def _save(self, sql):
        res = self.conn.execute(sql)
        self.conn.commit()
        return res.lastrowid

    def _retrieve(self, sql):
        cursor = self.conn.execute(sql)
        result = cursor.fetchall()
        return result


        # if cols is None:
        #     return rows
        #
        # # build a list of dict
        # res = []
        # for row in rows:
        #     res.append({k: v for k, v in zip(cols, row)})
        # if len(res) == 0:
        #     return None
        # elif len(res) == 1:
        #     return res[0]
        # else:
        #     return res

    def close(self):
        if self.conn is not None:
            self.conn.close()


if __name__ == '__main__':
    db = DBManager()
    db.drop_tables()
    db.create_tables()

