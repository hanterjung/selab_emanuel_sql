import logging

import nltk
from nltk.corpus import wordnet as wn
from nltk.parse.stanford import StanfordParser, StanfordDependencyParser

from emanual import constants
from emanual import settings
from emanual import database

logging.basicConfig(
    format="[%(name)s][%(asctime)s] %(message)s",
    handlers=[logging.StreamHandler()],
    level=logging.DEBUG
)
logger = logging.getLogger(__name__)


def _traverse(tree, elements, level=0):
    # rules of traversal stop
    label = tree.label()
    if (label != 'ROOT'
        and label != 'S' and  label != 'SBAR' and label != 'SBARQ'
        and label != 'SINV' and label != 'SQ'
        and label != 'PP' and label != 'VP'
        and label != 'FRAG'):
        logger.debug(tree)
        joined_text = (
            ' '.join(tree.leaves())
                .replace('-RRB-', ')')
                .replace('-LRB-', '(')
        )
        elements.append([joined_text, label])  # (element, pos)
        return

    for subtree in tree:
        if type(subtree) == nltk.tree.Tree:
            _traverse(subtree, elements, level=level + 1)


def analyze_document(document):
    # --------------------------------------
    # example sentences
    # --------------------------------------
    # sentences_str = "The vehicle has an electronic key release system. This system is to prevent ignition key removal."
    # sentences_str += " John likes Mary. He is 27 years old."
    # sentences_str += " Independent Mode : This mode directs rear seating airflow."

    parser = StanfordParser(path_to_jar=settings.STANFORDPARSER_PATH, path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)
    dep_parser = StanfordDependencyParser(path_to_jar=settings.STANFORDPARSER_PATH, path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)

    document_lines = document.split('\n')
    sentences = []
    for line in document_lines:
        sentences.extend(nltk.sent_tokenize(line))
    token_trees = parser.raw_parse_sents(sentences)

    # ----------------------------------
    # Sentence Tokenization
    # sentence to elements
    # ----------------------------------
    elements_list = []
    for sent, tree in zip(sentences, token_trees):
        logger.debug("-----------------------")
        logger.debug("Sentence: %s" % (sent))
        tree = next(tree)
        elements = []
        _traverse(tree, elements)
        elements_list.append(elements)

    # ----------------------------------
    # Dependency Parsing
    # ----------------------------------
    # her apple vs. her
    pronomial_words = ['this', 'those', 'these', 'the', 'that', 'he', 'she', 'him', 'her', 'it']
    dep_trees = dep_parser.raw_parse_sents(sentences)
    # iter sentences
    for idx_sentence, elements in enumerate(elements_list):
        tree = next(dep_trees)
        tree = next(tree)
        logger.debug("-----------------------")
        dep_results = [morpheme.split('\t') for morpheme in tree.to_conll(4).split('\n')]

        # iter the tokenized elements
        # an element may be a clause
        # the result of dependency parsing is a list of words
        # hence, need to split the clause
        idx_dep = 0
        for idx_element, element in enumerate(elements):
            words = element[0].split(' ')
            role = None
            pronomial = False
            # iter splited elements (word)
            for idx_word, word in enumerate(words):
                # get matched dep result
                dep_result = dep_results[idx_dep]
                if dep_result[0] != word:
                    # not matched. skip
                    role = word
                    break
                else:
                    idx_dep += 1

                # the role is composed of [element, pos, head, dep]
                # if the length of the dependency parsing result is 1, it may be the period
                if len(dep_result) > 1:
                    pos = dep_result[1]
                    dep_tag = dep_result[3]
                else:
                    pos = word
                    dep_tag = word
                if 'subj' in dep_tag:
                    role = constants.ELEMENT_SUBJECT
                elif 'obj' in dep_tag:
                    role = constants.ELEMENT_OBJECT
                elif pos.startswith('VB') and dep_tag == 'aux':
                    role = constants.ELEMENT_AUXILIARYVERB
                elif pos.startswith('VB'):
                    role = constants.ELEMENT_VERB
                elif pos.startswith('RB'):
                    role = constants.ELEMENT_ADVERB
                elif 'comp' in dep_tag or 'root' in dep_tag:
                    role = constants.ELEMENT_COMPLEMENT

                # determine pronomial element
                lower_word = word.lower()
                if idx_word == 0 and lower_word in pronomial_words:
                    if lower_word == "her":
                        if len(words) > 1:
                            pronomial = True
                    else:
                        pronomial = True

            # add role of element
            elements_list[idx_sentence][idx_element].extend([role, pronomial])
    logger.debug("Result: %s" % (elements_list))
    return sentences, elements_list


def parse_dependencies(document):
    dep_parser = StanfordDependencyParser(path_to_jar=settings.STANFORDPARSER_PATH,
                                          path_to_models_jar=settings.STANFORDPARSER_MODEL_PATH)

    document_lines = document.split('\n')
    sentences = []
    for line in document_lines:
        sentences.extend(nltk.sent_tokenize(line))

    words_list = []
    for sent in sentences:
        words_list.append(nltk.word_tokenize(sent))

    results = []
    dep_trees = dep_parser.raw_parse_sents(sentences)
    for trees in dep_trees:
        for tree in trees:
            results.append([morpheme.split('\t') for morpheme in tree.to_conll(4).split('\n')])

    fixed_results = []
    for result, words in zip(results, words_list):
        fixed_result = []
        result_idx = 0
        for w in words:

            result_word = None
            if result_idx < len(result):
                result_word = result[result_idx]
                if len(result_word) < 2:
                    result_idx += 1

            if w == result_word[0]:
                fixed_result.append(result_word)
                result_idx += 1
            else:
                fixed_result.append([w, None, None, None])
        fixed_results.append(fixed_result)

    return sentences, fixed_results


if __name__ == '__main__':
    document0 = """The vehicle has an electronic key release system. This system is to prevent ignition key removal."""
    elements_list = parse_dependencies(document0)
    exit()

    document0 = """The vehicle has an electronic key release system. This system is to prevent ignition key removal."""
    document1 = """If equipped, the engine can be started from outside of the vehicle.
Starting the Vehicle
1) Press and release Q on the RKE transmitter.
2) Immediately, press and hold / for at least four seconds or until the parking lamps flash.
3) Start the vehicle normally after entering.
When the vehicle starts, the parking lamps will turn on.
Remote start can be extended.
Canceling a Remote Start
To cancel a remote start, do one of the following:
- Press and hold / until the parking lamps turn off.
- Turn on the hazard warning flashers.
- Turn the vehicle on and then off. See Remote Vehicle Start -> 31."""
    document2 = """To lock or unlock a door manually:
- From the inside use the door lock knob on the window sill.
- From the outside turn the key toward the front or rear of the vehicle, or press the K or Q button on the Remote Keyless Entry (RKE) transmitter.
Power Door Locks
K : Press to unlock the doors.
Q : Press to lock the doors. See Power Door Locks -> 33."""
    document3 = """To open the liftgate the vehicle must be in P (Park). Press the touch pad under the liftgate handle and lift up. To close the liftgate, use the pull cup or pull strap as an aid.
Power Liftgate
If equipped with a power liftgate, the vehicle must be in P (Park) to operate it.
- Press and hold 8 on the Remote Keyless Entry (RKE) transmitter.
- Press O.
- Press the touch pad on the outside liftgate handle.
See Liftgate -> 35."""

    analyze_document(document0)